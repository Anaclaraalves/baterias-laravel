@extends('templates.base')

@section('conteudo')

<main>
    <br>
        <h1>Conclusões</h1>
   <br>



   
</br>
        <h2>Conclusão a resistência interna:</h2>
        <p>Após a realização do trabalho, podemos concluir que quanto maior a resistência interna da bateria, quer dizer que mais gasta ela está.</p>
        <h2>Conclusão sobre as pilhas/baterias:</h2>
        <p>Como a resistência interna da bateria representa o quão gasta ela está, podemos concluir que as baterias golite, elgin e duracell plus AA são as mais gastas.</p>
        <br>
        <h2>Conclusão geral sobre o trabalho:</h2>
</br>
        <h3>Programação em Automação:</h3>
        <p>Com a realização do trabalho, podemos ter um maior aprofundamento no estudo do CSS, Javascript e HTML e podemos realizar ligações entre todas estas linguagens de programação. O CSS é a folha de estilos, responsável por estilizar a página, o HTML é responsável pelas configurações gerais da página e o Javascript se encarrega de calcular as expressões. No caso, os cálculos foram da resistência de carga e resistência interna. </p>
        <h3>Eletroeletrônica:</h3>
        <p>Para a disciplina de eletroeletrônica, podemos calcular e medir na prática a tensão sem carga e a tensão com carga de pilhas ou baterias. Quanto à medida da tensão nominal, ela aparece escrita na carcaça da pilha ou bateria. A capacidade de corrente foi obtida através de pesquisas na internet. A reistẽncia de carga e a resistência interna foram calculadas por meio do javascript.</p>
    </main>

    @endsection

    @section('rodape')
        <h4>Rodapé da página principal</h4>
    @endsection
@extends('templates.base')

@section('conteudo')
    <main>
        <h1>Medições</h1>
        <hr>
        <h2>Resultados:</h2>
        <table class="table table-dark table-striped table-bordered" id="tbDados">
            <thead>
                <tr>
                    <th>Pilha/Bateria</th>
                    < th>Tensão Nominal (V)</>
                    <th>Capacidade de Corrente</th>
                    <th>Tensão sem carga (V)</th>
                    <th>Tensão com carga (V)</th>
                    <th>Resistência de carga(ohm)</th>
                    <th>Resistência interna (ohm)</th>
                </tr> 
            </thead>
            <tbody>
                @foreach($medicoes as $medicao)
                <tr>
                    <td>{{$medicao->pilha_bateria}}</td>
                    <td>{{number_format($medicao->tensao_nominal,1,'.','')}}</td>
                    <td>{{$medicao->capacidade_corrente}}</td>
                    <td>{{$medicao->tensao_sem_carga}}</td>
                    <td>{{$medicao->tensao_com_carga}}</td>
                    <td>{{$medicao->resistencia_carga}}</td>
                    <td>{{number_format($medicao->resistencia_interna,3,'.','')}}</td>
                </tr>
                @endforeach
           </tbody>
          
            
    </main>
    @endsection

    @section('rodape')
        <h4>Rodapé da página principal</h4>
    @endsection

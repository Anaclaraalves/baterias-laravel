@extends('templates.base')

@section('conteudo')
    <main>
        <h1>Turma 2D3 - Grupo 5</h1>
        <h2>Participantes</h2>
        <hr>
        <table class="table table-striped table-bordered" id="tabela1">
            <tr>
                <th>Matrícula</th>
                <th>Nome</th>
                <th>Função</th>
            </tr>
            <tr>
                <td>0072547</td>
                <td>Ana Clara Alves da Silva</td>.
                <td>Anotação e medição da tensão das pilhas e baterias</td>
            </tr>
            <tr>
                <td>0072557</td>
                <td>Leonardo Passos Sampaio</td>
                <td>Anotação e medição da tensão das pilhas e baterias</td>
            </tr>
            <tr>
                <td>0072530</td>
                <td>Mateus Rocha Oliveira</td>
                <td>Gerente e desenvolvedor css e javascript</td>
            </tr>
            <tr>
                <td>0066066</td>
                <td>Victor Henrique Duarte Maia</td>
                <td>Desenvolvedor html</td>
            </tr>
        </table>
        <img class="grupo" src="imgs/fotogrupo.jpg" alt="Componentes do grupo">
    </main>
   
@endsection

@section('rodape')
    <h4>Rodapé da página principal</h4>
@endsection